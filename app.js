var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors= require("cors")
var bodyParser=require('body-parser')
var usersRouter = require('./routes/users');
var auth =require("./routes/auth")
var app = express();
app.use(bodyParser.urlencoded({
    extended: true
  }));
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');
app.engine('ejs', require('ejs').__express);
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public/dist/econfigurateur')));

app.use(cors())
app.use('/users', usersRouter);
app.use('/auth', auth);

app.get('/*',(req,res)=>{
  res.sendFile('index.html',{root:'./dist/econfigurateur'});
});

module.exports = app;
