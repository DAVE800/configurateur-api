var express = require('express');
var router = express.Router();
var db = require("../configs/db")
const short = require('short-uuid');
const bcrypt = require("bcrypt");
/* GET users listing. */
router.get('/:id', function(req, res, next) {
  let user= JSON.stringify(req.params.id)
  let query= `SELECT * FROM users where users.user_id=${user}`
  db.query(query,(err,resp)=>{
    if(err){
      res.status(400).json(
        {
          status:false,
          statusCode:400,
          message:"Bad request"

        }
      );

    }else{
      res.status(200).json(
        {
          status:true,
          statusCode:200,
          message:"successful request",
          data:resp[0]

        }
      );
    }
  })
});


router.post('/', async (req, res, next) =>{
  let query = `INSERT INTO users(user_id,first_name,last_name,email,phone,post_code,address,city,password,age,company) VALUES ?`
  var user=short.generate()
  const salt = await bcrypt.genSalt(10);
  // now we set user password to hashed password
  var password = await bcrypt.hash(req.body.passord, salt);
  var data=[user,req.body.first_name,req.body.last_name,req.body.email,req.body.phone,req.body.postcode,req.body.address,req.body.city,password,req.body.age,req.body.company]
  db.query(query,[[data]],(err,resp)=>{
    if(err){
      console.log(err)
      res.status(400).json({
        statusCode:400,
        status:false,
        message:"bad request"
      })
    }else{
      res.status(201).json(
        {
         status:true,
         statusCode:201,
         message:"successful query",
         data:{user_id:user}
        })
    }
  })
});

module.exports = router;
