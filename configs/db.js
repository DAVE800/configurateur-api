require('dotenv').config()

const mysql = require('mysql');

const db = mysql.createConnection({     
     host:process.env.MYSQL_HOST, 
     user:process.env.MYSQL_USER ,
     password:process.env.MYSQL_ROOT_PASSWORD,
     database:process.env.MYSQL_DATABASE,
     port:process.env.MYSQL_PORT

});

 db.connect(function(err){
            
    if(err){
      console.log(err)
    
      throw err;
    
    }
    console.log('Connection established...')

 });

 module.exports=db;